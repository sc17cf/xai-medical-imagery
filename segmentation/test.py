import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from training import Trainer
from data.dataset import SegmentationDataGen
from tensorflow import keras
from data.utils import Encoder, PredictClass
from matplotlib import pyplot as plt
import matplotlib
import matplotlib.pyplot as plt


os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'

BATCH_SIZE = 1
IMAGE_SIZE = 256

IMAGES_DIR = '../../data/wrist_dataset/images'
MASKS_DIR = '../../data/wrist_dataset/truths'

image_paths = sorted(
    [
        os.path.join(IMAGES_DIR, fname)
        for fname in os.listdir(IMAGES_DIR)
        if fname.endswith(".png")
    ]
)
mask_paths = sorted(
    [
        os.path.join(MASKS_DIR, fname)
        for fname in os.listdir(MASKS_DIR)
        if fname.endswith(".png") and not fname.startswith(".")
    ]
)


print(f"Number of samples: {len(image_paths)}\n")

data_gen = SegmentationDataGen(len(image_paths), (IMAGE_SIZE, IMAGE_SIZE), image_paths, mask_paths, augmentation=False, random=False)

model = keras.models.load_model('saved/UNet-NoAug/model.h5', compile=False)
#model = keras.models.load_model('XNet_E25_ACC0.82_model.h5', compile=False)

encoder = Encoder()
pc = PredictClass()

for x, y in data_gen:
    pred = model.predict(
        x, batch_size=1,
    )

    for i in range(len(pred)):
        prediction = encoder.onehot_to_rgb(pred[i])
        mask = encoder.onehot_to_rgb(y[i])
        image = x[i]

        # see class for params
        crop_shape = [75, 115, 40, 80]
        try:
            _, cropped, pred_dist = pc.get_most_common_color(prediction, crop_shape[0],  crop_shape[1],  crop_shape[2],  crop_shape[3])
            pred_classes = pc.rgb_to_class(pred_dist)
            print(pred_classes)
            hist = pc.create_dist_hist(pred_classes)
        except:
            continue

        fig, ax = plt.subplots(2,3)
        ax[0, 0].imshow(image), ax[0, 0].set_title('Input Image')
        ax[0, 1].imshow(mask), ax[0, 1].set_title('Ground Truth Mask')
        ax[0, 2].imshow(prediction), ax[0, 2].set_title('Prediction')
        ax[1, 0].imshow(image[crop_shape[0]:crop_shape[1], crop_shape[2]:crop_shape[3]]), ax[1, 0].set_title('Fracture Region')
        ax[1, 1].imshow(cropped), ax[1, 1].set_title('Fracture Region from Prediction')
        fig.tight_layout()
        plt.show()
