import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from training import Trainer
from data.dataset import SegmentationDataGen
from tensorflow import keras
from data.utils import Encoder, PredictClass
from matplotlib import pyplot as plt
import matplotlib
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix, classification_report
import numpy as np
from data.utils import visualize_cm
import pandas as pd

os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'

BATCH_SIZE = 1
IMAGE_SIZE = 256

IMAGES_DIR = '../../data/wrist_dataset/images'
MASKS_DIR = '../../data/wrist_dataset/truths'

image_paths = sorted(
    [
        os.path.join(IMAGES_DIR, fname)
        for fname in os.listdir(IMAGES_DIR)
        if fname.endswith(".png")
    ]
)
mask_paths = sorted(
    [
        os.path.join(MASKS_DIR, fname)
        for fname in os.listdir(MASKS_DIR)
        if fname.endswith(".png") and not fname.startswith(".")
    ]
)


print(f"Number of samples: {len(image_paths)}\n")

data_gen = SegmentationDataGen(len(image_paths), (IMAGE_SIZE, IMAGE_SIZE), image_paths, mask_paths, augmentation=False, random=False)

# model = keras.models.load_model('saved/Unet-NoAug/model.h5', compile=False)
model = keras.models.load_model('XNet_E25_ACC0.82_model.h5', compile=False)

encoder = Encoder()
pc = PredictClass()

y_true = np.array([])
y_pred = np.array([])

encoder = Encoder()

for x, y in data_gen:
    pred = model.predict(
        x, batch_size=1,
    )

    for i in range(len(pred)):
        prediction = encoder.onehot_to_index(pred[i])
        mask = encoder.onehot_to_index(y[i])

        y_true = np.concatenate([y_true, mask.flatten()])
        y_pred = np.concatenate([y_pred, prediction.flatten()])

# Normalise CM
cm = confusion_matrix(y_true, y_pred)
cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

classes = ['Background', 'Carpals', 'Distal Phalanges', 'Medial Phalanges', 'Metacarpal', 'Overlap Radius Ulna', 'Proximal Phalanges', 'Radius', 'Ulna']
visualize_cm(cm, classes, title='Normalised Confusion Matrix')

report = classification_report(y_true, y_pred, target_names=classes)
print(report)

