import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'

from training import Trainer
from data.dataset import SegmentationDataGen
from sklearn.model_selection import train_test_split

from data.utils import Encoder
import numpy as np

from training.loss import weighted_categorical_crossentropy, weighted_jaccard_distance, dice_loss

LRATE = 0.0001
BATCH_SIZE = 9
KERNEL_SIZE = 3
FILTER_LIST =  [64,128,256, 512, 1024]
IMAGE_SIZE = 256
N_CLASSES = 9
CLASS_WEIGHTS = np.array([0.01, 0.8, 0.8, 0.8, 0.8, 0.8,0.8, 1.0, 1.0])

SEED = 750
TRAIN_VALID_SPLIT = 0.7

IMAGES_DIR = '../../data/wrist_dataset/images'
MASKS_DIR = '../../data/wrist_dataset/truths'

NO_EPOCHS = 7

LOSS = weighted_categorical_crossentropy(CLASS_WEIGHTS)
#LOSS = weighted_jaccard_distance(CLASS_WEIGHTS)
#LOSS = dice_loss()


image_paths = sorted(
    [
        os.path.join(IMAGES_DIR, fname)
        for fname in os.listdir(IMAGES_DIR)
        if fname.endswith(".png")
    ]
)
mask_paths = sorted(
    [
        os.path.join(MASKS_DIR, fname)
        for fname in os.listdir(MASKS_DIR)
        if fname.endswith(".png") and not fname.startswith(".")
    ]
)

train_images, valid_images = train_test_split(
    image_paths,
    random_state=SEED,
    train_size=TRAIN_VALID_SPLIT,
    shuffle=False)

train_masks, valid_masks = train_test_split(
    mask_paths,
    random_state=SEED,
    train_size=TRAIN_VALID_SPLIT,
    shuffle=False)

print(f"Number of samples: {len(train_images)}\n")
print(f"Number of valid samples: {len(valid_images)}\n")

train_data_gen = SegmentationDataGen(BATCH_SIZE, (IMAGE_SIZE, IMAGE_SIZE), train_images, train_masks, augmentation=True, random=True, multiplier=5)
valid_data_gen = SegmentationDataGen(BATCH_SIZE, (IMAGE_SIZE, IMAGE_SIZE), valid_images, valid_masks, augmentation=False, random=False)

training = Trainer.Trainer(
    generator = train_data_gen,
    valid_generator = valid_data_gen,
    no_epochs = NO_EPOCHS,
    kernel_size = KERNEL_SIZE,
    input_shape = (IMAGE_SIZE, IMAGE_SIZE, 3),
    filters = FILTER_LIST,
    lrate = LRATE,
    loss = LOSS,
)
