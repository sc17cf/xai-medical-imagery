import os 
import sys
import random

import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt

from tensorflow.keras.preprocessing.image import load_img

import tensorflow as tf
from tensorflow import keras
import matplotlib
import matplotlib.pyplot as plt

from random import randrange

from .utils import Encoder
from data.augmentations import augmentator

class SegmentationDataGen(keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self, batch_size, img_size, input_img_paths, target_img_paths, augmentation=True, random=True, multiplier=1):
        self.batch_size = batch_size
        self.img_size = img_size
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths
        self.encoder = Encoder()
        self.augmentation = augmentation
        self.random=random
        self.multiplier = multiplier

    def __len__(self):
        return len(self.target_img_paths)*self.multiplier // self.batch_size

    def __getitem__(self, idx):
        if self.augmentation:
            image, mask = self.get_rand_batch_with_aug()
        else:
            image, mask = self.get_batch_without_aug(idx)

        return image, mask


    def get_rand_batch_with_aug(self):
        """
        Generate a batch of random samples with augmentation applied with probability of 0.8
        """
        indices = np.asarray(range(0, len(self.target_img_paths)))
        np.random.shuffle(indices)

        for idx in range(0, len(indices), self.batch_size):
    
            batch_input_img_paths = self.input_img_paths[idx : idx + self.batch_size]
            batch_target_img_paths = self.target_img_paths[idx : idx + self.batch_size]

            image_batch = np.zeros((self.batch_size,) + self.img_size + (3,), dtype="int32")
            mask_batch = []
            
            # Generate batch
            for i in range(len(batch_input_img_paths)):
                img = load_img(batch_input_img_paths[i], target_size=self.img_size, interpolation='nearest')
                mask = load_img(batch_target_img_paths[i], target_size=self.img_size, interpolation='nearest')
                
                # 0.8 percent chance to be augmentation
                if random.randrange(10) > 2:
                    img, mask = augmentator(np.array(img), np.array(mask))
                else:
                    img, mask = np.array(img), np.array(mask)

                image_batch[i] = img
                ohe = self.encoder.rgb_to_onehot(mask)
                mask_batch.append(ohe.astype('float32') )

            
            mask_batch= np.array(mask_batch)

            return image_batch, mask_batch


    def get_batch_without_aug(self, idx):
        """
        Generate a batch of random samples without augmentations
        """
        indices = np.asarray(range(0, len(self.target_img_paths)))
        if self.random:
            np.random.shuffle(indices)
    
        for idx in range(0, len(indices), self.batch_size):
        
            batch_input_img_paths = self.input_img_paths[idx : idx + self.batch_size]
            batch_target_img_paths = self.target_img_paths[idx : idx + self.batch_size]

            image_batch = np.zeros((self.batch_size,) + self.img_size + (3,), dtype="int32")
            
            # Generate batch images
            for j, path in enumerate(batch_input_img_paths):
                img = load_img(path, target_size=self.img_size, interpolation='nearest')
                image_batch[j] = img
            
            # Generate batch ground truths
            mask_batch = []
            for j, path in enumerate(batch_target_img_paths):
                img = load_img(path, target_size=self.img_size, interpolation='nearest')
                ohe = self.encoder.rgb_to_onehot(np.array(img))
                mask_batch.append(ohe.astype('float32') )

            mask_batch= np.array(mask_batch)

            return image_batch, mask_batch
