import cv2
import numpy as np
import random as rng
import typing
import os
import sys
from random import randint
from matplotlib import pyplot as plt

class DataItem:
    def __init__(self) -> None:
        self.origin_path: str = None
        self.__segmentation_paths: typing.Dict[str, str] = {}

    def add_segmentation_file_path(self, name: str, path: str):
        self.__segmentation_paths[name] = path

    def set_origin_path(self, path: str):
        self.origin_path = path

    def get_seg_img(self, name: str):
        return

    def get_origin_img(self):
        return cv2.imread(self.origin_path)

    def get_seg_list(self):
        return self.__segmentation_paths


class Dataset:
    def __init__(self, path: str=None) -> None:
        self.index: typing.Dict[str, DataItem] = {}

    def build_index_from_path(self, path: str):
        origin_path = f'{path}/images'
        masks_non_binary_path = f'{path}/masks_non_binary'

        for file in os.listdir(origin_path):
            if file.endswith('.png'):
                dataitem: DataItem = DataItem()
                dataitem.set_origin_path(f'{origin_path}/{file}')

                for bone_class in os.listdir(masks_non_binary_path):
                    possible_path: str = f'{masks_non_binary_path}/{bone_class}/{file}'

                    if os.path.isfile(possible_path):
                        dataitem.add_segmentation_file_path(bone_class, possible_path)

            self.index[file] = dataitem


class SegGroundTruthLoader:
    def __init__(self) -> None:
        self.classes: typing.Dict[str, str] = {}
        self.ground_truths: object = []


    def load_classes(self, classes: [str], colour_map: [str]) -> None:
        """
        maps a class to a colour
        """

        if len(classes) != len(colour_map):
            raise Exception('Invalid colour map size: Colour map must be equal in size to the number of classes')

        for i in range(len(classes)):
            self.classes[classes[i]] = colour_map[i]


    def get_classes(self, path: str) -> [str]:
        classes = []
        for f in os.scandir(path):
           if f.is_dir():
               classes.append(f.name)

        return classes

    def get_n_distinct_colours(self, n: int) -> [str]:
        colours = [
            [120, 0, 230], # pink [0, 160, 255]     -> overlap_radius_ulna
            [0, 230, 60], # green [0, 230, 60]      -> proximal_phalanges
            [60, 255, 255], # yellow [60, 255, 255] -> medial_phalanges
            [240, 60, 60], # blue [240, 60, 60]     -> carpals
            [60, 60, 240], # red [60, 60, 240]      -> metacarpal
            [240, 240, 60], # cyan [240, 240, 60]   -> ulna
            [255, 0, 255], # purple [255, 0, 255]   -> distal_phalanges
            [0, 160, 255] # orange [0, 160, 255]    -> radius
        ]
        
        if n > len(colours):
            raise Exception('Not enough colours')
        
        return colours[0:n]


    def create_groud_truth(self, data_item: DataItem):
        seg_list = data_item.get_seg_list()
        original = data_item.get_origin_img()
        height, width, channels = original.shape 

        ground_truth = np.zeros((height,width,3), np.uint8)

        for key, value in seg_list.items():
            if key in self.classes.keys():
                class_colour = self.classes[key]

                segmentation_class = cv2.imread(value)
                segmentation_class[np.where((segmentation_class==[255,255,255]).all(axis=2))] = class_colour

                ground_truth = np.maximum.reduce([ground_truth,segmentation_class])

        return ground_truth

    def show_groud_truth(self, original, ground_truth):

        plt.subplot(1, 2, 1)
        plt.imshow(cv2.resize(ground_truth, (0,0), fx=0.2, fy=0.2))
        plt.subplot(1, 2, 2)
        plt.imshow(cv2.resize(original, (0,0), fx=0.2, fy=0.2))

        plt.show()


        cv2.waitKey(0)

    def save_ground_truth(self, name: str, img):
        cv2.imwrite(f'wrist_dataset/truths/{name}', img)

    def colour_map_from_dict(self):
        values = list(self.classes.values())
        values.insert(0, [0, 0, 0])
        mapping = {tuple(c): t for c, t in zip(values, range(len(values)))}

        return mapping


if __name__=='__main__':
    ground_truth_generator = SegGroundTruthLoader()

    classes = ground_truth_generator.get_classes('wrist_dataset/masks_non_binary')
    
    colour_map = ground_truth_generator.get_n_distinct_colours(len(classes))
    ground_truth_generator.load_classes(classes, colour_map)

    dataset = Dataset()
    dataset.build_index_from_path('wrist_dataset')

    """
    for key, value in dataset.index.items():
        print(key, value)
        ground_truth = ground_truth_generator.create_groud_truth(value)
        ground_truth_generator.save_ground_truth(key, ground_truth)

    """
    print(colour_map)
    


