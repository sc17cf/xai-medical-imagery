import random
from collections import Counter

import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.utils import to_categorical
from matplotlib import pyplot as plt


def shuffle_together_simple(images, labels, bodyparts):

    c = list(zip(images,labels, bodyparts))
    shuffle(c)
    images, labels, bodyparts = zip(*c)    
    images = np.asarray(images)
    labels = np.asarray(labels)
    bodyparts = np.asarray(bodyparts)
    
    return images, labels, bodyparts

def shuffle_together(images, labels, bodyparts, filenames):

    c = list(zip(images,labels, bodyparts,filenames))
    shuffle(c)
    images, labels, bodyparts, filenames = zip(*c)    
    images = np.asarray(images)
    labels = np.asarray(labels)
    bodyparts = np.asarray(bodyparts)
    filenames = np.asarray(filenames)
    
    return images, labels, bodyparts, filenames


def random_crop(x, y, permin, permax):
    """
    Given permitted x,y cords, created a random crop of the image
    """

    h, w, _ = x.shape
    per_h = random.uniform(permin, permax)
    per_w = random.uniform(permin, permax)
    crop_size = (int((1-per_h)*h),int((1-per_w)*w))

    rangew = (w - crop_size[0]) // 2 if w>crop_size[0] else 0
    rangeh = (h - crop_size[1]) // 2 if h>crop_size[1] else 0
    offsetw = 0 if rangew == 0 else np.random.randint(rangew)
    offseth = 0 if rangeh == 0 else np.random.randint(rangeh)
    cropped_x = x[offseth:offseth+crop_size[0], offsetw:offsetw+crop_size[1], :]
    cropped_y = y[offseth:offseth+crop_size[0], offsetw:offsetw+crop_size[1], :]
    resize_x = cv2.resize(cropped_x, (h, w), interpolation=cv2.INTER_CUBIC)
    resize_y = cv2.resize(cropped_y, (h, w), interpolation=cv2.INTER_NEAREST)
    if cropped_y.shape[-1] == 0:
        return x, y
    else:
        return np.reshape(resize_x,(h,w,3)), resize_y


class Encoder:
    def __init__(self):
        self.color_dict = { #### RGB mapping
            0: (0, 0, 0),      # background
            1: (230, 0, 120),  # carpals
            2: (60, 230, 0),   # distal_phalanges
            3: (255, 255, 60), # medial_phalanges
            4: (60, 60, 240),  # metacarpal
            5: (240, 60, 60),  # overlap_radius_ulna
            6: (60, 240, 240), # proximal_phalanges
            7: (255, 0, 255),  # radius
            8: (255, 160, 0),  # ulna
        }

    def rgb_to_onehot(self, rgb_arr):
        """ 
        Takes a RGB image (H, W, 3) and converts it to a OHE format (H, W, 3)
        """
        num_classes = len(self.color_dict)
        shape = rgb_arr.shape[:2]+(num_classes,)
        arr = np.zeros( shape, dtype=np.int8 )
        for i, cls in enumerate(self.color_dict):
            arr[:,:,i] = np.all(rgb_arr.reshape( (-1,3) ) == self.color_dict[i], axis=1).reshape(shape[:2])
        
        return arr

    def onehot_to_rgb(self, onehot):
        """ 
        Takes a OHE (H, W, C) and converts it to a RGB image (H, W, 3)
        """
        single_layer = np.argmax(onehot, axis=-1)
        output = np.zeros( onehot.shape[:2]+(3,) )
        for k in self.color_dict.keys():
            output[single_layer==k] = self.color_dict[k]
            
        return np.uint8(output)

    def onehot_to_index(self, onehot):
        """ 
        Takes a OHE (H, W, C) and converts it to a integer indexed matrix
        with each position representing a pixel.
        """
        single_layer = np.argmax(onehot, axis=-1)
        output = np.zeros( onehot.shape[:2]+(1,) )
        for k in self.color_dict.keys():
            output[single_layer==k] = k
            
        return np.uint8(output)


class PredictClass:
    def __init__(self):
        self.rgb_dict = {
            '230,0,120':  'Carpals',
            '60,230,0':   'Distal Phalanges',
            '255,255,60': 'Medial_Phalanges',
            '60,60,240':  'Metacarpal',
            '240,60,60':  'Overlap Radius Ulna',
            '60,240,240': 'Proximal Phalanges',
            '255,0,255':  'Radius',
            '255,160,0':  'Ulna'
        }

    def get_most_common_color(self, img, y1, y2, x1, x2):

        # top left pixel (y-axis) and bottom left pixel (x-axis) + h/w
        # easy change to work with params of heatmap that are passed.
        crop = img[y1:y2, x1:x2] # [y:y+h, x:x+w]
        
        rgb_total = []
        w, h, _ = crop.shape

        for x in range(0, w):
            for y in range(0, h):
                r,g,b = crop[x, y]
                rgb_total.append(f'{str(r)},{str(g)},{str(b)}')     
        rgb_dict = Counter(rgb_total)
        rgb_dict.pop('0,0,0', None)

        return img, crop, rgb_dict.most_common(8)
        
    def rgb_to_class(self, rgb):
        class_pred = {}
        for i in range(len(rgb)):
            if not rgb[i][1] == 0: 
                class_pred[self.rgb_dict.get(str(rgb[i][0]))] = rgb[i][1]
        
        for k, v in self.rgb_dict.items():
            if v not in class_pred: class_pred[v] = 0

        return class_pred

    def create_dist_hist(self, pred_classes):
        k, v = pred_classes.keys(), pred_classes.values()
        plt.bar(k, np.divide(list(v), sum(v)), color=['tab:red'])
        plt.ylim(0,1), plt.ylabel('Percentage (%)', fontsize=11)
        plt.xlabel('Class', fontsize=11), plt.xticks(list(k), rotation='vertical')
        plt.tight_layout()

    def show(self, img, crop, pred_classes):
        fig, ax = plt.subplots(1, 2, 3)
        ax[0].plot(0,0), ax[0].set_title('Original'), ax[0].imshow(img)
        ax[1].plot(0,1), ax[1].set_title('Cropped'), ax[1].imshow(crop)
        ax[2].plot(0,2), ax[2].set_title('Probability Distribution'), ax[2].imshow(pred_classes)
        fig.tight_layout()
        plt.show()

def visualize_cm(cm, classes, title='Confusion matrix'):

    # Only use the labels that appear in the data
    title=title
    cmap=plt.cm.Blues

    fig, ax = plt.subplots(dpi = 200)
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    
    plt.tight_layout()
    plt.pause(0.001)
    plt.show()
