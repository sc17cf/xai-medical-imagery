import datetime
import os
import sys

import numpy as np
import tensorflow as tf

from keras import backend as K
from keras import losses
from keras.callbacks import (EarlyStopping, TensorBoard)
from keras.layers import *
from keras.models import Model, Sequential, load_model
from keras.optimizers import *
from keras.optimizers import Adam
from models import SegNetBasic, UNet, UNet2, XNet

print(f"GPUs Available: {tf.config.list_physical_devices('GPU')}\n")
class Trainer:
    
    def __init__(self, generator, valid_generator, no_epochs, kernel_size, filters, input_shape, lrate = 1e-4,  loss = 'categorical_crossentropy'):
        self.generator = generator
        self.valid_generator = valid_generator
        self.kernel_size = kernel_size
        self.filters = filters
        self.input_shape = input_shape
        self.lrate = lrate
        self.no_epochs = no_epochs
        self.loss = loss

        self.save_path = f'model-{datetime.datetime.now().strftime("%Y%m%d-%H%M%S")}/'

        self.compile()
        self.fit()

    def compile(self):
        self.model = XNet.model(input_shape = self.input_shape, kernel_size = self.kernel_size, filter_depth = self.filters)
        self.model.compile(optimizer = Adam(learning_rate = self.lrate), loss = self.loss, metrics = ['accuracy'])

    def fit(self):
        """
        Initialise the callbacks and fit the model
        """
        earlystop = EarlyStopping(monitor="val_loss", min_delta = 0, patience = 20, verbose = 1, mode = 'min') 

        log_dir = f'saved/{self.save_path}'
        tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

        self.model.fit(self.generator, steps_per_epoch=len(self.generator), epochs = self.no_epochs, callbacks = [tensorboard_callback, earlystop], validation_data = self.valid_generator )
        self.model.save(f'saved/{self.save_path}/model.h5') 
