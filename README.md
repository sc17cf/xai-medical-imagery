# Assistive XAI Medical Imagery

## Fracture Detection
The fracture detection source code can be found in the detection folder in the top level directory.

### Requirements
The relevant requirements for running the project are found in the requirements.txt file in the gitlab repository. 
To install all these requirements run the following command:
```
pip install -r requirements.txt --no-cache-dir
```

### Dataset

A dataset must be supplied to train the model on. A suitable dataset to use is the MURAv1.1 dataset.

MURAv1.1 - request access from https://stanfordmlgroup.github.io/competitions/mura/. This may take some time to recieve the link via email. This must then be unzipped.
The MURA dataset must be formatted to work with the fracture detection system. This can be done using the format-mura.py file found in the detection source code using:
```
python format_mura.py *MURA_DATSET_PATH* dataset/MURA
```
The dataset must then be moved into the specified ‘dataset’ directory.

SHAIP - Unavailable for public access.

### Setup and Execution
The config.py file requires updating with individual environment specifics. The following environment variables need to be set:
* DATSET_PATH - the directory path containing the normal and abnormal folders with x-ray images inside.
* WEIGHT_DIR - the directory path containing the pretrained weights.
* MODEL_DIR - the directory path  to store the zipped model checkpoints.
If using the MURA dataset as specified above these variables do not need to be changed.

The run.py file is used to modify the functionality of the system. The following variables can be changed to modify behaviour:
name - the name of the model to save or load.
* image_path - optional. The path to an X-ray image to evaluate the model on.
* action - the action to perform. Valid options include:
  - “eval” - evaluate the named model on a given image and show the heatmap.
  - “train” - train the specified model using data specified in the config.py file
  - “test” - test the named model on the validation set, plot the evaluation heuristics.
* load_model - whether to load a model from MODEL_DIR with the name given in ‘name’ or to create a new model.

Navigate to detection/src directory and use the following command from the terminal window: 
```
python run.py
```

## Bone Segmentation

### Requirements :
The relevant requirements for running the project are found in the requirements.txt file in the gitlab repository. 
To install all these requirements run the following command:
```
pip install -r requirements.txt --no-cache-dir
```

### Setup and Execution
As the train.py cannot be used without the dataset it is not possible to run the code at this time. 


