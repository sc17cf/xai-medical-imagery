#Code for cropping heatmapped image based on the colour of area around activation.


from PIL import Image, ImageFilter
import numpy as np



def cropmap(heatmap,original):
    im = Image.open(heatmap).convert('RGB')
    na = np.array(im)
    orig = na.copy()


    xray = Image.open(original).convert('RGB')
    xr = np.array(xray)
    orig2 = xr.copy()
    redY, redX = np.where(np.all(na == [222, 158, 214], axis=2))  #most intense region
    if len(redY)==0 and len(redX)==0:
        print("checking next most intense region")
        redY, redX = np.where(np.all(na == [206,109,189], axis=2))    #second most intense region

    top, bottom = min(redY), max(redY)
    left, right = min(redX), max(redX)
    print(top, bottom, left, right)
    print(top,bottom,left,right)
    # Extract Region of Interest
    ROI = na[top:bottom, left:right]
    ogROI = xr[top:bottom, left:right]
    Image.fromarray(ROI).save('heatmap-cropped.png')
    Image.fromarray(ogROI).save('heatmap-croppedOriginal.png')
    bounding_box = [[top,left],[top,right],[bottom,left],[bottom,right]]
    print(bounding_box)
    return bounding_box
