import torch
from dataset_loader import Dataset
from torchvision import utils
import time
import copy

from config import device

def train_model(model, data, max_epochs):
    since = time.time()

    best_acc = 0.0
    epoch_loss = {  'train': 0.0,
                    'val': 0.0}
    epoch_acc = {   'train': 0.0,
                    'val': 0.0}

    for epoch in range(model.epoch, max_epochs):
        print('Epoch {}/{}'.format(epoch, max_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.model.train()  # Set model to training mode
            else:
                model.model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in data.data_loaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                model.optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model.model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = model.criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        model.optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            if phase == 'train':
                model.scheduler.step()

            epoch_loss[phase] = running_loss / data.dataset_sizes[phase]
            epoch_acc[phase] = running_corrects.double() / data.dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss[phase], epoch_acc[phase]))

        

        # save the best model
        model.statsrec[0].append(epoch_loss['train'])
        model.statsrec[1].append(epoch_loss['val'])
        model.statsrec[2].append(epoch_acc['val'])
        model.accuracy = epoch_acc['val']

        model.epoch += 1
        print()

        if epoch_acc['val'] > best_acc:
            best_acc = epoch_acc['val']
            model.save(best=True)
            print("Saved new best model with accuracy {}".format(best_acc))
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
