from torchvision import transforms
from torch.autograd import Variable
from torch.nn import functional as F
from torch import topk
import numpy as np

from .SaveFeatures import SaveFeatures

class GradCAM():
  
    def getCAM(feature_conv, weight_fc, class_idx):
        _, nc, h, w = feature_conv.shape
        cam = weight_fc[class_idx].dot(feature_conv.reshape((nc, h*w)))
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_img = cam / np.max(cam)
        return [cam_img]

    def preProcessImage(image):
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )

        preprocess = transforms.Compose([
            transforms.Resize((224,224)),
            transforms.ToTensor(),
            normalize
        ])

        tensor = preprocess(image)
        
        return tensor 

    def prepareGradCAM(image_tensor, model, final_layer):
          
        prediction_var = Variable((image_tensor.unsqueeze(0)), requires_grad=True)
        
        model.eval()

        activated_features = SaveFeatures(final_layer)

        prediction = model(prediction_var)
        pred_probabilities = F.softmax(prediction, 1).data.squeeze()
        activated_features.remove()

        weight_softmax_params = list(model._modules.get('fc').parameters())
        weight_softmax = np.squeeze(weight_softmax_params[0].data.numpy())

        class_idx = topk(pred_probabilities,1)[1].int()

        return activated_features, weight_softmax, class_idx