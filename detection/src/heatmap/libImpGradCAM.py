# Library installation: pip install pytorch-gradcam
from PIL import Image
import cv2
import torch
import skimage.transform
from torchvision import transforms
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
import numpy as np

from gradcam.utils import visualize_cam
from gradcam import GradCAM, GradCAMpp
from dataset_loader import data_transforms

def libImp(model, target_layer, image, class_idx):
    """
    Generate and save a heatmap (heatmap.png) of the activation of the passed PIL image.
    """
    # prepare input image
    torch_img = data_transforms['test'](image)
    result_image = transforms.ToPILImage()(torch_img)
    result_image.save('original_image.png', "PNG")
    torch_img = torch_img.unsqueeze(0)

    model.eval()

    # get GradCAM output result 
    gradcam = GradCAM(model, target_layer)
    mask, _ = gradcam(torch_img, class_idx)
    #heatmap, result = visualize_cam(mask, torch_img)
 
    heatmap = (255 * mask.squeeze()).type(torch.uint8).cpu().numpy()
    #result_image = transforms.ToPILImage()(heatmap)
    #result_image.save('heatmap.png', "PNG")

    plt.imsave('heatmap.png', heatmap, cmap="tab20b")
    #plt.savefig('heatmap.png', bbox_inches="tight", pad_inches=0)
    #result_image = transforms.ToPILImage()(heatmap)
    #result_image.save('heatmap.png', "PNG")
    """
    fig = plt.figure(frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(image, aspect='auto')
    ax.imshow(heatmap, alpha=0.5, cmap="tab20b")
    fig.savefig("slight-overlay.png")
    """
    orig = Image.open("original_image.png").convert("RGB")
    overlay = Image.open("heatmap.png").convert("RGB")

    overlay_img = Image.blend(orig,overlay,0.5)
    overlay_img.save("slight-overlay.png","PNG")
