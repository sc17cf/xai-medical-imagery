import torch
from torchvision import datasets,transforms,utils
import numpy as np
import os

current = os.getcwd()
print(os.listdir(current))
print(current)

data_transforms = {
    'train': transforms.Compose([transforms.Resize(500),
                                    transforms.CenterCrop(250),
                                    transforms.ToTensor()]),
    'val':transforms.Compose([transforms.Resize(500),
                                    transforms.CenterCrop(250),
                                    transforms.ToTensor()]),
    }


data_dir = os.path.join(current,'FORMATTEDMURA')

image_datasets = {x:datasets.ImageFolder(os.path.join(data_dir,x),data_transforms[x]) for x in ['train','val']}
dataloaders = {x:torch.utils.data.DataLoader(image_datasets[x],batch_size=100,shuffle=True,num_workers = 0) for x in ['train','val']}
loader = dataloaders['train']

print("Length of loader - ",len(loader))
print("Calculating values...")


mean = 0.0

for images, _ in loader:
    batch_samples = images.size(0)
    images = images.view(batch_samples, images.size(1),-1)
    mean += images.mean(2).sum(0)
    print(mean)

mean = mean / len(loader.dataset)
print("MEAN COMPLETE")

var = 0.0

for images, _ in loader:
    batch_samples = images.size(0)
    images = images.view(batch_samples, images.size(1),-1)
    var += ((images - mean.unsqueeze(1))**2).sum([0,2])
std = torch.sqrt(var /(len(loader.dataset)*224*224))

print(mean)
print(std)
