from shutil import copy
import os
import argparse
import sys

# get mura top level directory path and path for output dataset
parser = argparse.ArgumentParser(description='Format MURAv1.1 dataset')
parser.add_argument('mura_path', type=str,
                    help='the path to the MURA dataset')
parser.add_argument('new_path', type=str,
                    help='the path to create the new dataset location')

args = parser.parse_args()
print(args)

# verify MURA path
if not (os.path.isdir(args.mura_path)):
    print("ERROR: MURA path not a directory")
    sys.exit(0)

# make new dataset folder structure
if not os.path.exists(args.new_path):
    os.makedirs(args.new_path)
    print("Created new directory {}".format(args.new_path))

    train_path = os.path.join(args.new_path, 'train')
    os.makedirs(train_path)
    print("Created new directory {}".format(train_path))

    train_abnormal_path = os.path.join(train_path, 'abnormal')
    os.makedirs(train_abnormal_path)
    print("Created new directory {}".format(train_abnormal_path))

    train_normal_path = os.path.join(train_path, 'normal')
    os.makedirs(train_normal_path)
    print("Created new directory {}".format(train_normal_path))

    val_path = os.path.join(args.new_path, 'val')
    os.makedirs(val_path)
    print("Created new directory {}".format(val_path))
    
    val_abnormal_path = os.path.join(val_path, 'abnormal')
    os.makedirs(val_abnormal_path)
    print("Created new directory {}".format(val_abnormal_path))

    val_normal_path = os.path.join(val_path, 'normal')
    os.makedirs(val_normal_path)
    print("Created new directory {}".format(val_normal_path))

else:
    if not os.path.isdir(args.new_path):
        print("ERROR: new path is not a directory, {}".format(args.new_path))
        sys.exit(0)

image_path_lists = {'train': 'train_image_paths.csv',
                    'val': 'valid_image_paths.csv'}

for key, csv_path in image_path_lists.items():
    positive_class_path = os.path.join(args.new_path, key, 'abnormal')
    negative_class_path = os.path.join(args.new_path, key, 'normal')
    full_csv_path = os.path.join(args.mura_path, csv_path)

    with open(full_csv_path) as fp:
        files = fp.read().splitlines()
        for f in files:
            image_path = f.split(',')[0]
            # parse image path for body part
            if not image_path.find('XR_WRIST') == -1:
                if image_path.find('positive') == -1:
                    class_value = 0
                else:
                    class_value = 1

                if class_value == 1:
                    new_image_path = os.path.join(positive_class_path, "".join(image_path.split('/')))
                else:
                    new_image_path = os.path.join(negative_class_path, "".join(image_path.split('/')))
                
                copy(image_path, new_image_path)
